package pl.sdacademy.day13.spisLudnosci;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class HumanList {
    private static Scanner scanner = new Scanner(System.in);
    private static List<Human> humans = new ArrayList<>();

    protected static void addHuman() {
        System.out.println("Podaj imie: ");
        String name = getString();
        System.out.println("Podaj nazwisko: ");
        String surname = getString();
        System.out.println("Podaj wiek: ");
        int age = getNumber();
        humans.add(new Human(name, surname, age));
        System.out.println("Osoba dodana.");
    }

    protected static void deleteHuman() {
        System.out.println("Podaj imie: ");
        String name = getString();
        System.out.println("Podaj nazwisko: ");
        String surname = getString();
        System.out.println("Podaj wiek: ");
        int age = getNumber();
        Human human = new Human(name, surname, age);
        if (humans.contains(human)) {
            humans.remove(human);
            System.out.println("Osoba usunięta");
        } else {
            System.out.println("Osoba ine istnieje.");
        }
    }

    protected static void VeryfiHuman() {
        System.out.println("Podaj index 1. osoby: ");
        Human human = humans.get(getNumber());
        System.out.println("Podaj index 2. osoby: ");
        Human human1 = humans.get(getNumber());
        if (human.equals(human1)) {
            System.out.println("To ta sama osba");
        } else {
            System.out.println("To inne osoby");
        }
    }

    protected static void printList() {
        for (Human human : humans) {
            System.out.println(human.toString());
        }
    }

    protected static int getNumber()  {
        int number;
        try {
            number = scanner.nextInt();
        }catch (InputMismatchException e){
            System.out.println("Podaj numer: ");
            number=getNumber();
        }
        return number;
    }

    private static String getString() {
        String string = scanner.next();
        return string;
    }
}
