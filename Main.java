package pl.sdacademy.day13.spisLudnosci;

public class Main {

    public static void main(String[] args) {
        int opcino;
        HumanList.printList();
        do {
            printMenu();
            opcino = HumanList.getNumber();
            menu(opcino);

        } while (opcino != 0);
    }

    private static void printMenu() {
        System.out.println("1 - Dodaj człowieka, \n2 - Usuń człowieka, \n3 - Porównaj ludzi, \n4 - Wyświetl liste, \n0 - Zakończ");
    }

    private static void menu(int number) {
        switch (number) {
            case 1:
                HumanList.addHuman();
                break;
            case 2:
                HumanList.deleteHuman();
                break;
            case 3:
                HumanList.VeryfiHuman();
                break;
            case 4:
                HumanList.printList();
                break;
            case 0:
                break;

        }
    }

}
